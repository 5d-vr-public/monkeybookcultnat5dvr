using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RectTransform))]
public class BookManager : MonoBehaviour
{
	[SerializeField] GameObject firstIndexPageUI;
	[SerializeField] GameObject secondIndexPageUI;

	void Start()
	{
		Initialize();
		Book.Instance.OnFlip.AddListener(OnFlipIndexUICheck);
	}

	void Update()
	{

	}

	void OnFlipIndexUICheck()
	{
		Debug.Log(Book.Instance.currentPage);
		if (Book.Instance.currentPage == 20)
		{
			firstIndexPageUI.SetActive(true);
			secondIndexPageUI.SetActive(false);
		}
		else if (Book.Instance.currentPage == 4)
		{
			firstIndexPageUI.SetActive(false);
			secondIndexPageUI.SetActive(false);
		}
		else
		{
			firstIndexPageUI.SetActive(false);
			secondIndexPageUI.SetActive(false);
		}
	}
	void Initialize()
	{
		firstIndexPageUI.SetActive(false);
		secondIndexPageUI.SetActive(false);

		RectTransform thisRectTransform = GetComponent<RectTransform>();
		RectTransform bookRectTransform = Book.Instance.GetComponent<RectTransform>();
		// These four properties are to be copied
		thisRectTransform.anchorMin = bookRectTransform.anchorMin;
		thisRectTransform.anchorMax = bookRectTransform.anchorMax;
		thisRectTransform.anchoredPosition = bookRectTransform.anchoredPosition;
		thisRectTransform.sizeDelta = bookRectTransform.sizeDelta;

	}
}
