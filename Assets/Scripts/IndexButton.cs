using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Button))]

public class IndexButton : MonoBehaviour
{
	public int pageNumber;
	Button indexButton;
	void Start()
	{
		indexButton = GetComponent<Button>();
		indexButton.onClick.AddListener(FlipPage);
	}

	void Update()
	{

	}

	void FlipPage()
	{
		Book.Instance.currentPage = pageNumber - 4;
		FindObjectOfType<AutoFlip>().FlipRightPage();
	}
}
