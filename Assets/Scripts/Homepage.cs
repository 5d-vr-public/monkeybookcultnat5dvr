using UnityEngine;
using TouchScript;
using TouchScript.Pointers;
using TouchScript.Utils;
using TouchScript.Behaviors;
using TouchScript.Gestures;
using TouchScript.Gestures.TransformGestures;
using System.Collections.Generic;
using TouchScript.Utils.Attributes;
using UnityEngine.Profiling;
using UnityEngine.UI;
using UnityEngine.Sprites;
using UnityEngine.Video;
using UnityEngine.SceneManagement;
public class Homepage : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    private void OnEnable()
    {
        if (TouchManager.Instance != null)
        {
            // When click
            // TouchManager.Instance.TouchesMoved += HandleTouchesMoved; // When move the touch
            //  TouchManager.Instance.TouchesEnded += HandleTouchesEnded; // When end the touch


            TouchManager.Instance.PointersPressed += pointersPressedHandler;
            TouchManager.Instance.PointersReleased += HandleTouchesBegan;

            //TouchManager.Instance.PointersUpdated += HandleTouchestest;
        }

    }

    private void OnDisable()
    {
        if (TouchManager.Instance != null)
        {
            TouchManager.Instance.PointersPressed -= pointersPressedHandler;
            TouchManager.Instance.PointersReleased -= HandleTouchesBegan;
            // TouchManager.Instance.PointersAdded += pointersPressedHandler;

            //  TouchManager.Instance.PointersUpdated -= HandleTouchestest;

        }
    }
    private void HandleTouchesBegan(object sender, PointerEventArgs e)
    {
        //  Debug.Log("mouse up");


    }
    private void pointersPressedHandler(object sender, PointerEventArgs e)
    {

        Vector2 point;
        RaycastHit hitz;

        foreach (var pointer in e.Pointers)
        {

            point = new Vector2(pointer.Position.x, pointer.Position.y);
            // Debug.Log(point.ToString());

            if (Physics.Raycast(Camera.main.ScreenPointToRay(new Vector3(pointer.Position.x, pointer.Position.y, 0f)), out hitz))
            {
                //   Debug.Log("mouse down");
                if (hitz.collider.tag == "destormaser_buttons")
                {
                    SceneManager.LoadScene("Destor");
                }
                //damerwatan_buttons
                //estratgyarabiconefour_buttons
                //estratgyarabictwotwo_buttons
                //estratgyenglishtwotwo_buttons
                //home_buttons
                //moasher_buttons
                //nation_buttons
                //partrie_buttons

           
                if (hitz.collider.tag == "home_buttons")
                {
                     Debug.Log("home_buttons");
                    SceneManager.LoadScene("home");
                }
                if (hitz.collider.tag == "damerwatan_buttons")
                {
                    SceneManager.LoadScene("damerwatan");
                }
                if (hitz.collider.tag == "estratgyarabiconefour_buttons")
                {
                    SceneManager.LoadScene("estratgyarabiconefour");
                }
                if (hitz.collider.tag == "estratgyarabictwotwo_buttons")
                {
                    SceneManager.LoadScene("estratgyarabictwotwo");
                }
                if (hitz.collider.tag == "estratgyenglishtwotwo_buttons")
                {
                    SceneManager.LoadScene("estratgyenglishtwotwo");
                }
                if (hitz.collider.tag == "moasher_buttons")
                {
                    SceneManager.LoadScene("moasher");
                }
                if (hitz.collider.tag == "nation_buttons")
                {
                    SceneManager.LoadScene("nation");
                }
                if (hitz.collider.tag == "partrie_buttons")
                {
                    SceneManager.LoadScene("partrie");
                }
                //SceneManager.LoadScene("damerwatan");
                //SceneManager.LoadScene("estratgyarabiconefour");
                //SceneManager.LoadScene("estratgyarabictwotwo");
                //SceneManager.LoadScene("estratgyenglishtwotwo");
                //SceneManager.LoadScene("home");
                //SceneManager.LoadScene("moasher");


                //SceneManager.LoadScene("");
                //SceneManager.LoadScene("");
                //SceneManager.LoadScene("");
            }
        }
    }

    public void partrie_v()
    {
        SceneManager.LoadScene("partrie");
    }

    public void home()
    {
        SceneManager.LoadScene("home");
    }

    public void Destor_v()
    {
        SceneManager.LoadScene("Destor");
    }


    public void damerwatan_v()
    {
        SceneManager.LoadScene("damerwatan");
    }

    public void nation_v()
    {
        SceneManager.LoadScene("nation");
    }

    public void moasher_v()
    {
        SceneManager.LoadScene("moasher");
    }


    public void estratgyenglishtwotwo_v()
    {
        SceneManager.LoadScene("estratgyenglishtwotwo");
    }


    public void estratgyarabictwotwo_v()
    {
        SceneManager.LoadScene("estratgyarabictwotwo");
    }

    public void estratgyarabiconefour_v()
    {
        SceneManager.LoadScene("estratgyarabiconefour");
    }
    //SceneManager.LoadScene("MainScene");
    // Update is called once per frame
    void Update()
    {
        
    }
}
